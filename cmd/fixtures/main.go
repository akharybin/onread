package main

import (
	"channelz/model"
	"channelz/module/db"
	"channelz/module/fetcher"
	"channelz/module/config"
	"fmt"
)

type Chann struct {
	Name     string
	Tags     []*model.Tag
	Category *model.Category
	Language model.NullString
	Feeds    []string
}

func main() {
	config.InitConfig()
	db := db.Conn()

	categories := make(map[string]*model.Category)

	for _, categoryName := range []string{"programming", "news"} {
		category := &model.Category{Name: categoryName, Slug: categoryName}
		db.Create(category)
		categories[categoryName] = category
	}

	tags := make(map[string]*model.Tag)

	for _, tagName := range []string{"go", "golang", "php", "symfony", "postgres", "postgresql", "новости"} {
		tag := model.Tag{Name: model.ToNullString(tagName)}
		db.Create(&tag)
		tags[tagName] = &tag
	}

	channels := []Chann{
		Chann{
			"golang",
			[]*model.Tag{tags["go"], tags["golang"]},
			categories["programming"],
			model.ToNullString("en"),
			[]string{
				"http://golangweekly.com/rss/1cf9gng6",
				"https://golangnews.com/index.xml",
				"https://blog.gopheracademy.com/index.xml",
				"http://dave.cheney.net/feed",
				"https://www.youtube.com/feeds/videos.xml?channel_id=UC9ZNrGdT2aAdrNbX78lbNlQ",
				"http://eli.thegreenplace.net/feeds/go.atom.xml",
			},
		},
		Chann{
			"postgresql",
			[]*model.Tag{tags["postgres"], tags["postgresql"]},
			categories["programming"],
			model.ToNullString("en"),
			[]string{
				"http://www.depesz.com/feed/",
				"http://adpgtech.blogspot.com/feeds/posts/default",
				"http://blog.2ndquadrant.com/feed/",
				"http://bonesmoses.org/feed/",
				"https://blog.acolyer.org/feed/",
				"http://www.thatguyfromdelhi.com/feeds/posts/default",
				"http://michael.otacoo.com/feeds/postgresql.xml",
				"http://www.cybertec.at/author/hans-juergen-schoenig/feed/",
				"http://blog.pgaddict.com/rss",
				"http://blog.2ndquadrant.com/comments/feed/",
				"http://pgsnake.blogspot.com/feeds/posts/default",
				"http://rhaas.blogspot.com/feeds/posts/default",
			},
		},
		Chann{
			"новости",
			[]*model.Tag{tags["новости"]},
			categories["news"],
			model.ToNullString("ru"),
			[]string{
				"https://meduza.io/rss/all",
			},
		},
	}

	user := model.User{Email: model.ToNullString("admin@admin.com")}
	db.Create(&user)

	for _, chann := range channels {
		channel := model.Channel{
			Name:     chann.Name,
			Language: chann.Language,
			Users:    []model.User{user},
			Tags:     chann.Tags,
			Category: *chann.Category,
		}

		db.FirstOrCreate(&channel, model.Channel{Name: chann.Name})

		for _, feedUrl := range chann.Feeds {
			fmt.Println(feedUrl)
			feed := fetcher.FetchFeed(feedUrl)
			fmt.Println(feed.Title)
			feed.Category = *chann.Category
			feed.Channels = []model.Channel{channel}
			feed.Tags = chann.Tags

			db.FirstOrCreate(&feed, model.Feed{FeedLink: feed.FeedLink})
		}
	}

//		channelToFeedsMap := map[string]map[string]interface{}{
//			"tech-news": {
//				"language": "en",
//				"tags": []string{"tech news"},
//				"feeds": map[string][]string{
//					"http://www.techworld.com/news/rss": []string{"tech news", "it news"},
//				},
//			},
//			"programming": {
//				"language": "en",
//				"tags": []string{"programming"},
//				"feeds": map[string][]string{
//					"http://thenewstack.io/blog/feed/": []string{"distibuted services", "development", "programming"},
//					"http://feeds.soundcloud.com/users/soundcloud:users:172286536/sounds.rss": []string{"podcast", "distibuted services", "development", "programming"},
//	 			},
//			},
//			"php": {
//				"language": "en",
//				"tags": []string{"php"},
//				"feeds": map[string][]string{
//					"http://us4.campaign-archive2.com/feed?u=135396dd31790e834e8629b45&id=af4af98573": []string{"php news", "php weekly"},
//					"https://simplecast.com/podcasts/1356/rss": []string{"laravel", "podcast", "php"},
//					"http://feeds.feedburner.com/symfony/blog": []string{"symfony", "php"},
//				},
//			},
//		}
//
}
