package main

import (
	"channelz/module/db"
	"channelz/module/fetcher"
	"channelz/module/config"
	"channelz/model"
	"sync"
	"runtime"
	"fmt"
)

func init() {
	runtime.GOMAXPROCS(4)
}

func main() {
	var wg sync.WaitGroup

	config.InitConfig()
	db := db.Conn()
	feeds := []model.Feed{}
	db.Where(model.Feed{ID: 1}).Find(&feeds)

	wg.Add(len(feeds))

	for _, f := range feeds {
		fmt.Println(f.FeedLink)
		go func(f model.Feed) {
			defer wg.Done()
			fetcher.PollFeed(&f, 30)
		}(f)
	}

	wg.Wait()
}
