package main

import (
	"channelz/model"
	"channelz/module/config"
)

func main() {
	config.InitConfig()
	model.Migrate()
}
