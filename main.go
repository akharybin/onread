package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/engine/standard"
	"github.com/labstack/echo/middleware"
	"channelz/handler"
	"channelz/module/config"
)

func init() {
	config.InitConfig()
}

func main() {
	e := echo.New()
	e.SetDebug(true)
	e.Post("/signin/google", handler.SigninGoogle)
	e.Post("/signup/google", handler.SignupGoogle)
	e.Use(middleware.CORS())

	r := e.Group("/api")
	//r.Use(middleware.JWT([]byte("secret")))
	r.GET("/channels", handler.Channels)
	r.GET("/channels/:name", handler.Channel)
	r.GET("/feeds", handler.Feeds)
	r.GET("/items", handler.Items)

	e.Run(standard.New(":1323"))
}