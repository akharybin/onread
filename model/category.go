package model

type Category struct {
	ID       uint   `gorm:"primary_key" json:"-"`
	Name     string `gorm:"name:varchar(255);not null" json:"name"`
	Slug     string `gorm:"slug:varchar(255);not null" json:"slug"`
	Parent   *Category `json:"parent_category"`
	ParentID uint  	`json:"-"`
}
