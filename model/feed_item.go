package model

type FeedItem struct {
	ID          uint        `gorm:"primary_key" json:"-"`
	GUID        string      `gorm:"type:varchar(255);not null;unique" json:"guid"`
	Link        string      `gorm:"type:varchar(255);not null" json:"link"`
	Title       NullString  `gorm:"type:varchar(255);not null" json:"title"`
	Description NullString  `json:"description" json:"description"`
	Content     NullString  `json:"content" json:"content"`
	Enclosures  []Enclosure `json:"enclosures"`
	Categories  Jsonb       `gorm:"type:jsonb" json:"categories"`
	PublishedAt NullTime    `json:"-"`
	UpdatedAt   NullTime    `json:"-"`
	Feed        Feed        `json:"-"`
	FeedID      uint        `json:"-"`
}
