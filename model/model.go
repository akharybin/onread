package model

import (
	"channelz/module/db"
	"database/sql"
	"database/sql/driver"
	"errors"
	"github.com/pquerna/ffjson/ffjson"
	"time"
)

func Migrate() {
	db.Conn().AutoMigrate(&User{}, &Feed{}, &Channel{}, &FeedItem{}, &Tag{}, &Enclosure{}, &Category{})
}

type NullTime struct {
	time.Time
	Valid bool
}

type Jsonb []string

type NullString struct {
	sql.NullString
}

func (v NullString) MarshalJSON() ([]byte, error) {
	if v.Valid {
		return ffjson.Marshal(v.String)
	} else {
		return ffjson.Marshal(nil)
	}
}

func (nt *NullTime) Scan(value interface{}) error {
	nt.Time, nt.Valid = value.(time.Time)
	return nil
}

func (nt NullTime) Value() (driver.Value, error) {
	if !nt.Valid {
		return nil, nil
	}
	return nt.Time, nil
}

func (j Jsonb) Value() (driver.Value, error) {
	valueString, err := ffjson.Marshal(j)

	return string(valueString), err
}

func (j *Jsonb) Scan(value interface{}) error {
	if err := ffjson.Unmarshal(value.([]byte), &j); err != nil {
		return err
	}

	return nil
}

func ToNullTime(value string) (NullTime, error) {
	if value == "" {
		return NullTime{Valid: false}, errors.New("value is empty")
	}

	for _, layout := range []string{time.RFC1123, time.RFC1123Z, time.RFC822, time.RFC822Z} {
		if t, err := time.Parse(layout, value); err == nil {
			return NullTime{t, true}, nil
		}
	}

	return NullTime{Valid: false}, errors.New("Not valid value")
}

func ToNullString(value string) NullString {
	return NullString{sql.NullString{value, len(value) > 0}}
}
