package model

type Enclosure struct {
	ID         uint     `gorm:"primary_key";json:"-"`
	URL        string   `gorm:"type:varchar(255);not null";json:"url"`
	Length     int      `gorm:"type:int;not null"json:"length,omitempty"`
	Type       string   `json:"type"`
	FeedItem   FeedItem `json:"-"`
	FeedItemID uint     `json:"-"`
}
