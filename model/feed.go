package model

type Feed struct {
	ID          uint       `gorm:"primary_key" json:"id"`
	Language    NullString `gorm:"char(2)" json:"language"`
	Title       string     `gorm:"type:varchar(512);not null" json:"title"`
	Description NullString `gorm:"type:varchar(512)" json:"description"`
	Link        NullString `gorm:"type:varchar(255);" json:"siteUrl"`
	FeedLink    string     `gorm:"type:varchar(255);not null;unique" json:"url"`
	ImageUrl    NullString `gorm:"type:varchar(255)"`
	ImageTitle  NullString `gorm:"type:varchar(255)"`
	FaviconUrl  NullString `gorm:"type:varchar(255)" json:"faviconUrl"`
	CreatedAt   NullTime   `json:"-"`
	UpdatedAt   NullTime   `json:"-"`
	PublishedAt NullTime   `json:"-"`
	Categories  Jsonb      `gorm:"type:jsonb"`
	Enabled     bool       `gorm:"DEFAULT:false" json:"-"`
	Items       []FeedItem `json:"-"`
	Channels    []Channel  `gorm:"many2many:channel_feeds;" json:"-"`
	Tags        []*Tag     `gorm:"many2many:feed_tags;" json:"tags"`
	Category    Category   `json:"category"`
	CategoryID  uint       `json:"-"`
}
