package model

type Channel struct {
	ID         uint       `gorm:"primary_key" json:"id"`
	Language   NullString `gorm:"not null;char(2);index:channel_language_idx" json:"language"`
	CreatedAt  NullTime   `json:"-"`
	UpdatedAt  NullTime   `json:"-"`
	Name       string     `gorm:"not null;unique;varchar(20);index:channel_name_idx" json:"name"`
	Feeds      []Feed     `gorm:"many2many:channel_feeds;"  json:"feeds"`
	Users      []User     `gorm:"many2many:user_channels;"  json:"-"`
	Properties Jsonb      `gorm:"type:jsonb" json:"-"`
	Tags       []*Tag     `gorm:"many2many:channel_tags;" json:"tags"`
	Category   Category   `json:"category"`
	CategoryID uint       `json:"-"`
}

type Tag struct {
	ID   uint       `gorm:"primary_key" json:"-"`
	Name NullString `gorm:"not null;unique" json:"name"`
}
