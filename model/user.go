package model

import (
	"channelz/module/db"
)

type User struct {
	ID        uint `gorm:"primary_key" json:"id"`
	CreatedAt NullTime `json:"-"`
	UpdatedAt NullTime `json:"-"`
	Email     NullString `gorm:"not null;unique" json:"email"`
	FirstName NullString `json:"firstName"`
	LastName  NullString `json:"lastName"`
	ImageUrl  NullString `json:"imageUrl"`
	Role      uint       `grom:"not null" json:"-"`
	Channels  []Channel  `gorm:"many2many:user_channels;" json:"channels"`
}

type UserProfile struct {
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	ImageUrl  string `json:"imageUrl"`
	Email     string `json:"email"`
}

func CreateUser() *User {
	return &User{Channels: make([]Channel, 0)}
}

func SignUpUser(user *User, profile *UserProfile) error {
	user.FirstName = ToNullString(profile.FirstName)
	user.LastName = ToNullString(profile.LastName)
	user.ImageUrl = ToNullString(profile.ImageUrl)
	user.Email = ToNullString(profile.Email)

	return FindOrCreateUser(user, profile.Email)
}

func FindUserByEmail(user *User, email string) error {
	return db.Conn().Where("email = ?", email).Preload("Channels").First(user).Error
}

func FindOrCreateUser(user *User, email string) error {
	return db.Conn().Where("email = ?", email).Preload("Channels").FirstOrCreate(user).Error
}