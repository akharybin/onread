package handler

import (
	"github.com/labstack/echo"
	"google.golang.org/api/oauth2/v2"
	"net/http"
	"channelz/model"
	"github.com/dgrijalva/jwt-go"
	"encoding/json"
)

var httpClient = &http.Client{}

type ErrResponse struct {
	Type string `json:"type"`
	Error string `json:"error"`
}

func verifyToken(idToken string) (*oauth2.Tokeninfo, error) {
	oauth2Service, err := oauth2.New(httpClient)
	tokenInfoCall := oauth2Service.Tokeninfo()
	tokenInfoCall.IdToken(idToken)
	tokenInfo, err := tokenInfoCall.Do()

	if err != nil {
		return nil, err
	}

	return tokenInfo, nil
}

func createUserToken(user *model.User) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)
	token.Claims["user"] = user
	t, err := token.SignedString([]byte("secret"))

	if err != nil {
		return "", err
	}

	return t, nil
}

func SigninGoogle(c echo.Context) error {
	tokenInfo, err := verifyToken(c.FormValue("id_token"))

	if err != nil {
		return c.JSON(http.StatusForbidden, err)
	}

	user := model.CreateUser()
	user.Email = model.ToNullString(tokenInfo.Email)
	err = model.FindUserByEmail(user, tokenInfo.Email)

	if err != nil {
		return c.JSON(http.StatusNotFound, ErrResponse{"UserNotFound", "User with email " + tokenInfo.Email + " not found"})
	}

	var t string
	t, err = createUserToken(user)

	return c.JSON(http.StatusOK, map[string]string{
		"token": t,
	})
}

func SignupGoogle(c echo.Context) error {
	tokenInfo, err := verifyToken(c.FormValue("id_token"))

	if err != nil {
		return c.JSON(http.StatusForbidden, err)
	}

	profile := &model.UserProfile{}
	err = json.Unmarshal([]byte(c.FormValue("profile")), profile)

	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	profile.Email = tokenInfo.Email
	user := model.CreateUser()
	err = model.SignUpUser(user, profile)

	if err != nil {
		return err
	}

	var t string
	t, err = createUserToken(user)

	return c.JSON(http.StatusOK, map[string]string{
		"token": t,
	})
}

