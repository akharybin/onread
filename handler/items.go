package handler

import (
	"github.com/labstack/echo"
	"net/http"
	"channelz/module/db"
	"channelz/model"
	"strconv"
)

func Items(c echo.Context) error {
	page, err := strconv.Atoi(c.QueryParam("page"))
	channelName := c.QueryParam("channel")
	if err != nil {
		page = 1
	}

	channel := model.Channel{}
	db.Conn().Where(model.Channel{Name: channelName}).Preload("Feeds").First(&channel)

	feedIds := []uint{}
	for _, feed := range channel.Feeds {
		feedIds = append(feedIds, feed.ID)
	}

	items := []model.FeedItem{}
	db.Conn().Where("feed_id IN (?)", feedIds).Preload("Enclosures").Order("published_at desc").Limit(21).Offset((page-1)*20).Find(&items)

	response := map[string]interface{}{"items": items}
	if len(items) == 21 {
		response["nextId"] = items[20].ID
	}

	return c.JSON(http.StatusOK, response)
}
