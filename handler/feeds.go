package handler

import (
	"github.com/labstack/echo"
	"net/http"
	"channelz/module/db"
	"channelz/model"
	"strconv"
)

func Feeds(c echo.Context) error {
	page, err := strconv.Atoi(c.QueryParam("page"))
	if err != nil {
		page = 1
	}

	feeds := []model.Feed{}
	db.Conn().Limit(10).Offset((page-1)*10).Find(&feeds)

	return c.JSON(http.StatusOK, feeds)
}
