package handler

import (
	"github.com/labstack/echo"
	"net/http"
	"channelz/module/db"
	"channelz/model"
	"strconv"
)

func Channels(c echo.Context) error {
	page, err := strconv.Atoi(c.QueryParam("page"))
	if err != nil {
		page = 1
	}

	channels := []model.Channel{}
	db.Conn().Preload("Category").Preload("Category.Parent").Preload("Tags").Limit(10).Offset((page-1)*10).Find(&channels)

	return c.JSON(http.StatusOK, channels)
}

func Channel(c echo.Context) error {
	ch := model.Channel{}
	db.Conn().Preload("Feeds").First(&ch, model.Channel{Name: c.Param("name")})

	return c.JSON(http.StatusOK, ch)
}