package db

import (
	"github.com/jinzhu/gorm"
	"github.com/labstack/gommon/log"
	_ "github.com/lib/pq"
	"sync"
	"github.com/spf13/viper"
)

var db *gorm.DB
var once sync.Once

func Conn() *gorm.DB {
	once.Do(func() {
		var err error
		db, err = gorm.Open("postgres", viper.Get("db_dsn"))
		if err != nil {
			log.Fatal(err)
		}
	})

	return db
}
