package jwt

import (
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/engine"
	"strings"
	"fmt"
)

const (
	SigningKey = "somethingsupersecret"
	BearerLength = 6
)

func NewJwt(claims map[string]interface{}) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)

	token.Header["typ"] = "JWT"
	token.Claims = claims

	return token.SignedString([]byte(SigningKey))
}

func ParseJwtFromRequest(request engine.Request) (*jwt.Token, error) {
	if authHeader := request.Header().Get("Authorization"); authHeader != "" {
		if len(authHeader) > 6 && strings.ToUpper(authHeader[0:7]) == "BEARER " {
			token, err := jwt.Parse(authHeader[7:], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
				}

				return []byte(SigningKey), nil
			})

			if err == nil && token.Valid {
				return token, nil
			}
		}
	}

	return nil, fmt.Errorf("Request must container authorization header")
}