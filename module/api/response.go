package api

const (
	ResourceNotFound = "ResourceNotFound"
	UserNotFound     = "UserNotFound"
	Unauthorized     = "Unauthorized"
	ValidationFailed = "ValidationFailed"
)

type Response struct {
	Type string `json:"type"`
	Message string `json:"message"`
	Errors map[string]interface{} `json:"data,omitempty"`
}
