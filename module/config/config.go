package config

import (
	"github.com/spf13/viper"
	"github.com/labstack/gommon/log"
)

func InitConfig() {
	viper.SetConfigType("yml")
	viper.SetConfigName("config")
	viper.AddConfigPath("$GOPATH/src/channelz")
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal(err)
	}
}
