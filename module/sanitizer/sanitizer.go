package sanitizer

import (
	"github.com/microcosm-cc/bluemonday"
)

func Sanitize(html string) string {
	return bluemonday.UGCPolicy().AddTargetBlankToFullyQualifiedLinks(true).Sanitize(html)
}
