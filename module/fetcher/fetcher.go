package fetcher

import (
	"github.com/mmcdole/gofeed"
	"channelz/model"
	"strconv"
	"log"
	"fmt"
	"time"
	"channelz/module/db"
	"channelz/module/sanitizer"
	"html"
	"github.com/jinzhu/gorm"
	"channelz/module/config"
)

var conn *gorm.DB
var fp = gofeed.NewParser()

func init() {
	config.InitConfig()
	conn = db.Conn()
}

func FetchFeed(url string) model.Feed {
	parsedFeed, err := fp.ParseURL(url)
	if err != nil {
		log.Println(err)
	}
	updateAt, _ := model.ToNullTime(parsedFeed.Updated)
	publishedAt, _ := model.ToNullTime(parsedFeed.Published)
	feed := model.Feed{
		Title: parsedFeed.Title,
		FeedLink: url,
		UpdatedAt: updateAt,
		PublishedAt: publishedAt,
		Categories: parsedFeed.Categories,
	}

	if parsedFeed.Image != nil {
		feed.ImageUrl = model.ToNullString(parsedFeed.Image.URL)
		feed.ImageTitle = model.ToNullString(parsedFeed.Image.Title)
	}

	return feed
}

func PollFeed(feed *model.Feed, interval uint) {
	lastGUID := ""

	for {
		fmt.Println("Start parse " + feed.FeedLink)
		parsedFeed, err := fp.ParseURL(feed.FeedLink)
		if err != nil {
			log.Println("Feed parsing failed: ", err)
		}
		for _, parsedItem := range parsedFeed.Items{
			if parsedItem.GUID == lastGUID {
				fmt.Println("No more new items detected.")
				break
			}

			fmt.Println("New item detected: " + parsedItem.GUID)
			saveFeedItem(feed, parsedItem)
		}

		if len(parsedFeed.Items) != 0 {
			lastGUID = parsedFeed.Items[0].GUID
		}

		time.Sleep(time.Minute)
	}
}

func saveFeedItem(feed *model.Feed, parsedItem *gofeed.Item) {
	itemUpdateAt, _ := model.ToNullTime(parsedItem.Updated)
	publishedAt, _ := model.ToNullTime(parsedItem.Published)

	item := model.FeedItem{
		Title: model.ToNullString(parsedItem.Title),
		UpdatedAt: itemUpdateAt,
		Description: model.ToNullString(sanitizer.Sanitize(html.UnescapeString(parsedItem.Description))),
		Content: model.ToNullString(sanitizer.Sanitize(parsedItem.Content)),
		Link: parsedItem.Link,
		GUID: parsedItem.GUID,
		PublishedAt: publishedAt,
		Categories: parsedItem.Categories,
		Feed: *feed,
	}

	fmt.Println(sanitizer.Sanitize(html.UnescapeString(parsedItem.Description)))

	for _, enclosure := range parsedItem.Enclosures {
		length, _ := strconv.Atoi(enclosure.Length)
		item.Enclosures = append(item.Enclosures, model.Enclosure{
			URL: enclosure.URL,
			Length: length,
			Type: enclosure.Type,
		})
	}

	conn.FirstOrCreate(&item, model.FeedItem{GUID: parsedItem.GUID})
}
