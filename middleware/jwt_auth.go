package middleware

import (
	"github.com/labstack/echo"
	"channelz/module/jwt"
	"net/http"
)

func JWTAuth() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return echo.HandlerFunc(func(c echo.Context) error {
			token, err := jwt.ParseJwtFromRequest(c.Request())
			if err != nil {
				return c.JSON(http.StatusForbidden, map[string]string{"error": "Not valid JWT token"})
			}

			c.Set("claims", token.Claims)

			return next(c)
		})
	}
}