package middleware

import (
	"github.com/labstack/echo"
//	"fmt"
//	"channelz/models"
)

const (
	RoleUser = 0
	RoleAdmin = 1
)

func Authorization(role int) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return echo.HandlerFunc(func (c echo.Context) error {
//			claims := c.Get("claims").(map[string]interface{})["user"].(models.User)
//			fmt.Println(claims)

			return next(c)
		})
	}
}